
import java.io.IOException;
import java.util.Scanner;

public class App {

    public static void main(String[] args) throws IOException {
        System.out.println("Pulse la tecla Enter para obtener la ultima cotizacion del dia");
        Scanner tecla = new Scanner(System.in);
        String enterKey = tecla.nextLine();
        String receivedKey = null;
        do {
            if (enterKey.isEmpty()) {
                try {
                    CotizacionRepository coinDeskCotizacionRepository = new CoinDeskCotizacionRepository();
                    CotizacionService cotizacionService = new CotizacionService(coinDeskCotizacionRepository);
                    System.out.println(cotizacionService.obtenerCotizacion());
                } catch (IOException exception) {
                    System.out.println(exception);
                }
                System.out.println("(Presione Enter para volver a consultar)");
                receivedKey = tecla.nextLine();
            }
        } while (receivedKey != null);
    }
}
