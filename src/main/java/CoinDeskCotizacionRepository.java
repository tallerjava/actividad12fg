
import java.text.SimpleDateFormat;
import java.util.Date;
import jodd.http.HttpException;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;
import org.json.JSONObject;

public class CoinDeskCotizacionRepository extends CotizacionRepository {

    private String url = "https://api.coindesk.com/v1/bpi/currentprice.json";

    public CoinDeskCotizacionRepository() {
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public Cotizacion obtenerCotizacion() throws NullPointerException {
        if (url.isEmpty()) {
            throw new HttpException("HttpException: la url no es valida.");
        }
        if (url == null) {
            throw new NullPointerException("NullPointerException: La url es null.");
        }
        if (!url.equals("https://api.coindesk.com/v1/bpi/currentprice.json")) {
            throw new IllegalArgumentException("IllegalArgumentException: La url es incorrecta.");
        } else {
            HttpResponse response = HttpRequest.get(url).send();
            JSONObject body = new JSONObject(response.body());
            String fecha = new SimpleDateFormat("d-M-yyyy HH:mm").format(new Date());
            String moneda = body.getJSONObject("bpi").getJSONObject("USD").getString("code");
            double precio = body.getJSONObject("bpi").getJSONObject("USD").getDouble("rate_float");
            Cotizacion cotizacion = new Cotizacion(fecha, moneda, precio);
            return cotizacion;
        }
    }
}
