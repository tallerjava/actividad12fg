
import java.io.IOException;

public abstract class CotizacionRepository {

    CotizacionRepository() {
    }

    public abstract Cotizacion obtenerCotizacion() throws IOException;
}
