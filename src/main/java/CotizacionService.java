
import java.io.IOException;

public class CotizacionService {

    private CotizacionRepository cotizacionRepository;

    public CotizacionService(CotizacionRepository cotizacionRepository) {
        this.cotizacionRepository = cotizacionRepository;
    }

    public Cotizacion obtenerCotizacion() throws IOException {
        return cotizacionRepository.obtenerCotizacion();
    }
}
