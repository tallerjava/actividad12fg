
import jodd.http.HttpException;
import org.junit.Test;
import static org.junit.Assert.*;

public class CoinDeskCotizacionRepositoryTest {

    public CoinDeskCotizacionRepositoryTest() {
    }

    @Test(expected = HttpException.class)
    public void obtenerCotizacion_sinUrl_httpException() throws HttpException {
        CoinDeskCotizacionRepository coinDeskCotizacionRepository = new CoinDeskCotizacionRepository();
        coinDeskCotizacionRepository.setUrl("");
        coinDeskCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = IllegalArgumentException.class)
    public void obtenerCotizacion_sinUrl_IllegalArgumentException() throws IllegalArgumentException {
        CoinDeskCotizacionRepository coinDeskCotizacionRepository = new CoinDeskCotizacionRepository();
        coinDeskCotizacionRepository.setUrl("https://api.coindesk.com/v1/bpi/currentprice.xml");
        coinDeskCotizacionRepository.obtenerCotizacion();
    }

    @Test(expected = NullPointerException.class)
    public void obtenerCotizacion_sinUrl_NullPointerException() throws NullPointerException {
        CoinDeskCotizacionRepository coinDeskCotizacionRepository = new CoinDeskCotizacionRepository();
        coinDeskCotizacionRepository.setUrl(null);
        coinDeskCotizacionRepository.obtenerCotizacion();
    }

    @Test
    public void obtenerCotizacion_urlValida_cotizacionObtenida() {
        CoinDeskCotizacionRepository coinDeskCotizacionRepository = new CoinDeskCotizacionRepository();
        assertNotNull(coinDeskCotizacionRepository.obtenerCotizacion());
    }
}
